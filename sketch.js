function removeFromArray(arr, el) {
    for (let i = arr.length; i >= 0; i--)
    {
        if (arr[i] === el){
            arr.splice(i, 1);
        }
    }
}

function heuristic(a, b) {
    return Math.abs(a.i - b.i) + Math.abs(a.j - b.j);
}

function randomInteger(max){
    return Math.floor(random() * ((max - 1)))
}

const cols = 15;
const rows = 15;
const grid = [cols];
let w, h;
let start;
let end;
let path = [];

const openList = [];
const closedList = [];

function Cell(i,j){
    this.i = i;
    this.j = j;
    this.f = 0;
    this.g = 0;
    this.h = 0;
    this.neighbors = [];
    this.previous = undefined;
    this.wall = false;

    if (random(1) < 0.2) {
        this.wall = true;
    }

    this.show = function (color) {
        fill(color); // White

        // if obstacle, then set it as black
        if (this.wall) {
            fill(0);
        }
        noStroke();
        rect(this.i*w, this.j*h, w - 1, h - 1);
    };

    this.addNeighbors = function (grid) {
        let i = this.i;
        let j = this.j;

        if (i < cols - 1){
            // South
            this.neighbors.push(grid[i+1][j]);
        }
        if (i > 0){
            // North
            this.neighbors.push(grid[i-1][j]);
        }
        if (j < rows - 1)
        {
            // East
            this.neighbors.push(grid[i][j+1]);
        }
        if (j > 0)
        {
            // West
            this.neighbors.push(grid[i][j-1]);
        }
    };
}


function setup() {
    createCanvas(400, 400);
    console.log('A*');

    w = width / cols;
    h = height / rows;

    // Making a 2D Array
    for (let i = 0; i < cols; i++) {
        grid[i] = new Array(rows);
    }

    // Assigning an Cell object to each position
    for (let i = 0; i < cols; i++) {
        for (let j = 0; j < rows; j++) {
            grid[i][j] = new Cell(i, j);
        }
    }

    // Assigning for each cell his neighbors
    for (let i = 0; i < cols; i++) {
        for (let j = 0; j < rows; j++) {
            grid[i][j].addNeighbors(grid)
        }
    }
    // Define start and end cells
    start = grid[randomInteger(cols)][randomInteger(rows)];
    end = grid[randomInteger(cols)][randomInteger(rows)];

    while (start.i === end.i && start.j === end.j)
    {
        start = grid[randomInteger(cols)][randomInteger(rows)];
        end = grid[randomInteger(cols)][randomInteger(rows)];
    }

    // Start and end can't be walls
    start.wall = false;
    end.wall = false;

    // Add start cell to openList
    openList.push(start);

    console.log(grid);
}

function draw() {

    let current;
    if (openList.length > 0){

        let lowestIndex = 0;
        for (let i = 0; i < openList.length; i++){
            if (openList[i].f < openList[lowestIndex].f) {
                lowestIndex = i;
            }
        }

        current = openList[lowestIndex];

        // Check if we reached the end
        if (current === end) {
            noLoop();
            console.log("WE NAILED IT BOIS, WOO");
            return;
        }

        // Delete the cell from openList
        removeFromArray(openList, current);
        // Add the nod to closedList
        closedList.push(current);

        let neighbors = current.neighbors;

        for (let i = 0; i < neighbors.length; i++){
            let neighbor = neighbors[i];

            // Check that neighbor is not in analysed yet (part of closedList)
            if (!closedList.includes(neighbor) && !neighbor.wall)
            {
                // Distance from start to a neighbor
                let temp_g = current.g + 1;

                // Check if openList contains neighbor
                if (openList.includes(neighbor)){
                    // if current neighbor g is better than the last neighbor
                    if (temp_g < neighbor.g) { // That's our path
                        neighbor.g = temp_g
                    }
                } else {
                    neighbor.g = temp_g;
                    openList.push(neighbor)
                }
                neighbor.h = heuristic(neighbor, end);
                neighbor.f = neighbor.h + neighbor.g;
                neighbor.previous = current;
            }
        }
    } else {
        console.log('DEAD END BOIS');
        // Aucun chemin possible
    }

    background(0);

    // Define every cells as white
    for (let i = 0; i < cols; i++){
        for (let j = 0; j < rows; j++){
            grid[i][j].show(255);
        }
    }

    // Define every cell from closedList as red
    for (let i = 0; i < closedList.length; i++){
        closedList[i].show(color(191, 244, 168));
    }

    // Define every cell from openList as green
    for (let i = 0; i < openList.length; i++) {
        openList[i].show(color(255, 244, 168));
    }

    // Find the path
    path = [];
    let temp = current;
    path.push(temp);
    while (temp.previous){
        path.push(temp.previous);
        temp = temp.previous;
    }

    for (let i = 0; i < path.length; i++) {
        path[i].show(color(188, 200, 255));
    }

    start.show(color(0, 255, 0));
    end.show(color(255, 0, 0));
}



